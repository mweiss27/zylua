print("Loading util.lua")
local log = require("./log")
require("./base64")

function endswith(str, val)
	if not str then return nil end
	if not val then return false end

	return str:find(val) == #str
end

function trim(str)
	if not str then return nil end
	return str:match("^%s*(.-)%s*$")
end

function encodePassword(password)
	if not password then return nil end

	return "Qö" .. base64(password)
end

function constructURL(base, params)
	if not base then
		log.error("base must not be nil in util.constructURL")
		return nil
	end

	if not params then params = {} end
	if not endswith(base, "?") then base = base .. "?" end

	local second = false
	for key, value in pairs(params) do
		if second then base = base .. "&" end

		base = base .. key .. "=" .. value
		second = true
	end

	return base
end