require("bit")

local bs = { [0] =
   'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P',
   'Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f',
   'g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v',
   'w','x','y','z','0','1','2','3','4','5','6','7','8','9','+','/',
}

local byte, rep = string.byte, string.rep

function base64(s)
   if s == nil then return nil end
   
   local pad = 2 - ((#s-1) % 3)
   s = (s..rep('\0', pad)):gsub("...", function(cs)
      local a, b, c = byte(cs, 1, 3)
      return bs[bit.rshift(a, 2)] .. bs[ bit.bor(bit.lshift((bit.band(a, 3)), 4), bit.rshift(b, 4)) ] .. bs[ bit.bor(bit.lshift((bit.band(b, 15)), 2), bit.rshift(c, 6)) ] .. bs[bit.band(c, 63)]
   end)
   return s:sub(1, #s-pad) .. rep('=', pad)
end