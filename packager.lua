print("Loading packager.lua")
local http = require("socket.http")
local log = require("./log")

require("./util")

function getLatestVersion(game)
	if not game then 
		log.error("game must not be nil in getLatestVersion")
		return nil
	end

	local base = game.packagerLink
	local params = {
		["do"] = "ver",
		["uv"] = "6.0.0.1",
		["source"] = "getLatestVersion"
	}

	local url = constructURL(base, params)

	body, code = http.request(url)
	if body and code then
		body = trim(body)
		if code == 200 then
			local match = body:match("^%d\.%d\.%d+$")
			if match then
				log.debug("Valid version!")
				return match
			else
				log.error("Invalid body on http.request in getLatestVersion: " .. body)
			end
		else
			log.error("Invalid response code on http.request in getLatestVersion: " .. code)
		end
	else
		log.error("body or code are nil from http.request")
	end
end
