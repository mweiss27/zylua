require("./config")

local packagerLinks = {
	["wow"] = "updater",
	["eso"] = "updater_eso"
}

local packagerFormat = SERVER_CONTENT .. "/%s/packager.php"
local WOW = {
	["packagerLink"] = packagerFormat:format(packagerLinks.wow)
}

local ESO = {
	["packagerLink"] = packagerFormat:format(packagerLinks.eso)
}

games = {
	["WOW"] = WOW,
	["ESO"] = ESO
}